package matrix

import (
	"fmt"
)

//Display uses "fmt" to print a 3x3 matrix with simple formating
func Display(M [3][3]float64) {
	fmt.Println("-----------------------------------------------------")
	for i := 0; i < 3; i++ {
		fmt.Printf("%f | %f | %f \n", M[i][0], M[i][1], M[i][2])
	}
	fmt.Print("\n")
}

//Input the content of a 3x3 matrix from stdin (CLI) using "fmt"
func Input(M *[3][3]float64) {
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			fmt.Println("-----------------------------------------------------")
			for k := 0; k < 3; k++ {
				if k == i {
					switch j {
					case 0:
						fmt.Printf("%s | %f | %f \n", "   ??   ", M[k][1], M[k][2])
						break
					case 1:
						fmt.Printf("%f | %s | %f \n", M[k][0], "   ??   ", M[k][2])
						break
					case 2:
						fmt.Printf("%f | %f | %s \n", M[k][0], M[k][1], "   ??   ")
						break
					}
				} else {
					fmt.Printf("%f | %f | %f \n", M[k][0], M[k][1], M[k][2])
				}
			}
			fmt.Print("\n")
			fmt.Print("Enter a float value : ")
			_, err := fmt.Scanf("%f", &M[i][j])
			if err != nil {
				fmt.Println(err)
			}
			//fmt.Printf("You have entered : %f \n\n", M[i][j])
			fmt.Print("\n\n")
		}
	}
}

//Multiplication takes two 3x3 matrix (M and N), returns R = M * N
//The Result matrix should be provided by reference
func Multiplication(M [3][3]float64, N [3][3]float64, Result *[3][3]float64) {
	// Result = M * N
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			for k := 0; k < 3; k++ {
				Result[i][j] += M[i][k] * N[k][j]
			}
		}
	}
}
