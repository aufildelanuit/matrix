package main

import (
	"fmt"
	"personal/matrix/lib"
)

func inputIterationNum() int {
	var n int
	fmt.Print("How many times should the matrix multiply itself : ")
	_, err := fmt.Scanf("%d", &n)

	if err != nil {
		fmt.Println(err)
	}

	return n
}

func matrixSelfMultiplication(M [3][3]float64, Result *[3][3]float64) {
	matrix.Multiplication(M, M, Result)
}

func main() {
	var M [3][3]float64
	matrix.Input(&M)
	matrix.Display(M)

	var n int
	n = inputIterationNum()
	fmt.Println("Multiplying", n, "times")

	var Result [3][3]float64
	var tempResult [3][3]float64
	var zero [3][3]float64

	if n >= 1 {
		if n > 1 {
			// initialises the first operation
			fmt.Println("1 : ")
			matrixSelfMultiplication(M, &Result)
			matrix.Display(Result)
			// then calculates for count >=2
			for count := 2; count <= n; count++ {
				fmt.Println(count, ": ")
				matrix.Multiplication(Result, M, &tempResult)
				Result = tempResult
				tempResult = zero
				matrix.Display(Result)
			}
		} else {
			// n == 1
			matrixSelfMultiplication(M, &Result)
			matrix.Display(Result)
		}
	} else {
		fmt.Println("number of iterations < 1; stopping.")
	}
}
